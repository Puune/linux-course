#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <readline/readline.h>
#include <readline/history.h>

#define FNAME "/t8_f_panu"
#define SNAME "/t8_s_panu"
#define MAX_LEN 80
#define M_SIZE 4096

struct henkilo
{
    char nimi[MAX_LEN];
    int ika;
};

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    printf("write: start\n");
    int fd;
    struct stat st;
    if((fd = shm_open(FNAME, O_CREAT | O_RDWR, 0660)) == -1)  //FD
        err_n_exit("write: shm");

    fstat(fd, &st);
    if(st.st_size < M_SIZE)
        ftruncate(fd, M_SIZE);

    sem_t *sem = sem_open(SNAME, O_CREAT , 0660, 1);    //SEMAPHORI
    if(sem == SEM_FAILED)
        err_n_exit("write: sem");

    char *buf;
    struct henkilo *hlo = (struct henkilo *) mmap(NULL, M_SIZE, PROT_WRITE, MAP_SHARED, fd, 0); //SHM
    if(hlo == MAP_FAILED)
        err_n_exit("write: map");
    
    buf = readline("Anna henkilön nimi:");
    strcpy(hlo->nimi, buf);
    free(buf);    
    buf = readline("Anna henkilön ikä:");
    int to_int = atoi(buf);
    hlo->ika = to_int;
    free(buf);

    sem_post(sem);

    munmap(hlo, M_SIZE);
    shm_unlink(FNAME);
    sem_unlink(SNAME);
    sem_close(sem);

    printf("write: exit\n");
    exit(EXIT_SUCCESS);
}
