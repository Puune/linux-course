#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>

#define FNAME "/t8_f_panu"
#define SNAME_W "/t8_sw_panu"
#define SNAME_R "/t8_sr_panu"
#define MAX_LEN 80
#define M_SIZE 4096

struct henkilo
{
    char nimi[MAX_LEN];
    int ika;
};

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    printf("read: start\n");
    struct stat st;
    int fd;
    if((fd = shm_open(FNAME, O_CREAT | O_RDWR, 0660)) == -1)  //FD
        err_n_exit("read: shm");

    fstat(fd, &st);
    if(st.st_size < M_SIZE)
        ftruncate(fd, M_SIZE);

    sem_t *sem_write = sem_open(SNAME_W, O_CREAT, 0660, 0);    //SEMAPHORE
    sem_t *sem_read = sem_open(SNAME_R, O_CREAT, 0660, 0);
    if(sem_write == SEM_FAILED | sem_read == SEM_FAILED)
        err_n_exit("read: sem");


    struct henkilo *hlo;
    while(1)
    {
        //printf("read: wait\n");   //D
        sem_wait(sem_read);
        //printf("read: cont\n");   //D

        hlo = mmap(NULL, M_SIZE, PROT_READ, MAP_SHARED, fd, 0); //SHM
        if(hlo == MAP_FAILED)
            err_n_exit("read: mmap");

        if(hlo->nimi[0] == (char *) NULL & hlo->ika == -1)
            break;

        printf("read print: %s %d \n", hlo->nimi, hlo->ika);
        sem_post(sem_write);
    }
    
    munmap(hlo, M_SIZE);
    sem_unlink(SNAME_R);
    sem_unlink(SNAME_W);
    shm_unlink(FNAME);
    sem_close(sem_write);
    sem_close(sem_read);

    printf("read: exit\n");
    exit(EXIT_SUCCESS);
}
