#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>

#define FNAME "/t8_f_panu"
#define SNAME "/t8_s_panu"
#define MAX_LEN 80
#define M_SIZE 4096

struct henkilo
{
    char nimi[MAX_LEN];
    int ika;
};

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    printf("read: start\n");
    struct stat st;
    int fd;
    if((fd = shm_open(FNAME, O_CREAT | O_RDWR, 0660)) == -1)  //FD
        err_n_exit("read: shm");

    fstat(fd, &st);
    if(st.st_size < M_SIZE)
        ftruncate(fd, M_SIZE);

    sem_t *sem = sem_open(SNAME, O_CREAT, 0660, 0);    //SEMAPHORE
    if(sem == SEM_FAILED)
        err_n_exit("read: sem");

    struct henkilo *hlo = mmap(NULL, M_SIZE, PROT_READ, MAP_SHARED, fd, 0); //SHM
    if(hlo == MAP_FAILED)
        err_n_exit("read: mmap");

    printf("read: wait\n");
    sem_wait(sem);
    printf("read: cont\n");

    printf("read print: %s %d \n", hlo->nimi, hlo->ika);

    munmap(FNAME, M_SIZE);
    sem_unlink(SNAME);
    shm_unlink(FNAME);
    sem_close(sem);
    printf("read: exit\n");
    exit(EXIT_SUCCESS);
}
