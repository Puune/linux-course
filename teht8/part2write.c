#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <readline/readline.h>
#include <readline/history.h>

#define FNAME "/t8_f_panu"
#define SNAME_W "/t8_sw_panu"
#define SNAME_R "/t8_sr_panu"
#define MAX_LEN 80
#define M_SIZE 4096
#define TRUE 1
#define FALSE 0

struct henkilo
{
    char nimi[MAX_LEN];
    int ika;
};

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    printf("write: start\n");
    int fd;
    struct stat st;
    if((fd = shm_open(FNAME, O_CREAT | O_RDWR, 0660)) == -1)  //FD
        err_n_exit("write: shm");

    fstat(fd, &st);
    if(st.st_size < M_SIZE)
        ftruncate(fd, M_SIZE);

    sem_t *sem_read = sem_open(SNAME_R, O_CREAT , 0660, 1);    //SEMAPHORI
    sem_t *sem_write = sem_open(SNAME_W, O_CREAT, 0660, 0);
    if(sem_read == SEM_FAILED | sem_write == SEM_FAILED)
        err_n_exit("write: sem");

    char *buf;
    struct henkilo *hlo;
    int cont = TRUE;
    while(cont)
    {
        hlo = (struct henkilo *) mmap(NULL, M_SIZE, PROT_WRITE, MAP_SHARED, fd, 0); //SHM
        if(hlo == MAP_FAILED)
            err_n_exit("write: map");
        
        buf = readline("Anna henkilön nimi (tai ctrl+d):");
        if(buf == 0)
        {
            printf("\n");
            hlo->nimi[0] = (char *) NULL;
            hlo->ika = -1;
            cont = FALSE;
        }
        else
        {
            strcpy(hlo->nimi, buf);
            free(buf);    
            buf = readline("Anna henkilön ikä:");
            int to_int = atoi(buf);
            hlo->ika = to_int;
            free(buf);
        }

        sem_post(sem_read);
        
        if(cont) sem_wait(sem_write);
    }

    munmap(hlo, M_SIZE);
    shm_unlink(FNAME);
    sem_unlink(SNAME_R);
    sem_unlink(SNAME_W);
    sem_close(sem_read);
    sem_close(sem_write);

    printf("\nwrite: exit\n");
    exit(EXIT_SUCCESS);
}
