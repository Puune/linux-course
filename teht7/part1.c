#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAX_STRING 1024

struct line
{
    char *line;
    struct line *previous;
};
struct line *stack = NULL;

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void push(char line[])
{
    //printf("PUSH %s\n", line); //Debug
    struct line *newline;

    if((newline=(struct line *)malloc(sizeof(struct line) +1)) == NULL)
        err_n_exit("Malloc");

    char *to_save = (char *)malloc(strlen(line)+1);
    strcpy(to_save, line);
    newline->line = to_save;
    newline->previous = stack;
    stack = newline;
    //printf("SAVE %s\n", stack->line); //DEBUG
}

int pop(char *value)
{
    if(stack == NULL)
        return 0;

    struct line *to_remove;    
    *value = malloc(strlen(stack->line) + 1);
    strcpy(value, stack->line);
    //printf("POP STACK %s\n", value);  //DEBUG
    to_remove = stack;
    stack = stack->previous;
    free(to_remove->line);
    free(to_remove);
    return 1;
}

int main(int argc, char const *argv[])
{
    char buf[MAX_STRING];
    int stop = 0;
    while (read(STDIN_FILENO, buf, MAX_STRING) > 0)
    {
        push(buf);
    }   
    
    char *value;
    while(pop(value) > 0)
    {
        printf("PRINT VALUE %s\n", value);
    }
    exit(EXIT_SUCCESS);
}
