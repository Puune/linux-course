#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <signal.h>

#define MAX_LEN 80
#define M_SIZE 4096

struct henkilo
{
    char nimi[MAX_LEN];
    int ika;
};

void handler(int sig)
{
    //char *msg = "SIG!\n";
    //write(STDOUT_FILENO, msg, strlen(msg));
}

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    int fd, pid;
    char *fname = "/t7_panu";

    if ((fd = shm_open(fname, O_CREAT | O_EXCL | O_RDWR | O_TRUNC, 0660)) == -1)
        err_n_exit("smh_open");

    ftruncate(fd, M_SIZE);

    if((pid = fork()) < 0)
        err_n_exit("fork");

    if(pid == 0) /* Lapsi */
    {
        //sig
        struct sigaction act;
        act.sa_handler = handler;
        act.sa_flags = 0;
        sigemptyset(&act.sa_mask);
        sigaction(SIGUSR1, &act, NULL);
        //sig

        pause();
        printf("child: unpause\n");

        struct henkilo *hlo = mmap(NULL, M_SIZE, PROT_READ, MAP_SHARED, fd, 0);
        if(hlo == MAP_FAILED)
            err_n_exit("child mmap");

        printf("child: %s %d\n", hlo->nimi, hlo->ika);

        munmap(hlo, M_SIZE);
        shm_unlink(fname);
        _exit(EXIT_SUCCESS);
    }
    else /* Vanhempi */ 
    {
        printf("parent: %d\n", pid);
        char buf[MAX_LEN];
        struct henkilo *hlo = (struct henkilo *) mmap(NULL, M_SIZE, PROT_WRITE, MAP_SHARED, fd, 0);
        if(hlo == MAP_FAILED)
            err_n_exit("parent map");


        printf("Anna henkilön nimi:\n");
        read(STDIN_FILENO, buf, MAX_LEN);
        strcpy(hlo->nimi, buf);    
        printf("Anna ika:\n");
        read(STDIN_FILENO, buf, MAX_LEN);
        int to_int = atoi(buf);
        hlo->ika = to_int;

        kill(pid, SIGUSR1);
        munmap(hlo, M_SIZE);

        printf("parent: quit\n");
    }

    exit(EXIT_SUCCESS);
}
