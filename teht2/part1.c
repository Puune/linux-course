#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char const *argv[])
{
    /* Initialisoi */
    if(argc != 2)
        err_n_exit("Only accepts 1 argument");

    char *buf, *ptr; // pointers for traversing opposite directions
    int fd; 
    off_t size, cur; 

    if((fd = open(argv[1], O_RDONLY)) < 0) 
        err_n_exit("Couldn't open file");

    size = lseek(fd, 0, SEEK_END); 
    if((buf = (char *)malloc(size)) == NULL) 
        err_n_exit("malloc");

    /* Aja */
    ptr = buf;
    size_t amount = 1;
    cur = size - 1; //size 1..X -> array 0..X-1
    while (cur >= 0) 
    {
        if(pread(fd, ptr, amount, cur) == -1)
            err_n_exit("pread");

        ptr++;
        cur--;
    }
    
    /* Tulosta */
    write(STDOUT_FILENO, buf, size);
    write(STDOUT_FILENO,"\n", 1);   //format

    /* Sulje */
    free(buf);
    close(fd);
    exit(EXIT_SUCCESS);
}


void err_n_exit (char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}