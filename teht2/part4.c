#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#define INPUT_WIDTH 10
#define TRUE 1
#define FALSE 0

int main(int argc, char const *argv[])
{
    int fdOut; 
    int summing = TRUE;
    struct stat sts;

    /* Initialisoi */
    if (argc != 2)
        err_n_exit("Give filename as argument");
    else if (stat(argv[1], &sts) == -1)
    {
        if ((fdOut = creat(argv[1], S_IRWXU)) < 0)
           err_n_exit("Could't create a file");

        summing = FALSE;
    }
    else
    {
        char prompt[] = "Overwrite(1) or Sum(2)? ";
        write(STDOUT_FILENO, prompt, sizeof(prompt));
        char ans = '\0';
        while(ans == '\0')
        {
            read(STDIN_FILENO, &ans, 1);
            if (ans == '1')
            {
                fdOut = open(argv[1], O_TRUNC | O_WRONLY);
                summing = FALSE;
            }
            else if (ans == '2')
            {
                fdOut = open(argv[1], O_RDWR);
            }
            else 
                ans = '\0';
        }
    }

    /* Aja */
    if (!summing)
    {
        char buf[INPUT_WIDTH];
        int stop = FALSE, status = -1;    
        int firstLine = !summing; // When not summing, start at file start
        write(STDOUT_FILENO, "Started reading\n", sizeof("Started reading\n"));
        while (!stop)
        {
            status = read(STDIN_FILENO, buf, INPUT_WIDTH);
            if(status == -1) //ERR
                err_n_exit("Error reading input");
            else if (status == 0) //EOF
            {
                write(STDOUT_FILENO, "Stopped reading\n", sizeof("Stopped reading\n"));
                stop = TRUE;
            }
            else
            {
                if(buf[0] == '\n')
                    continue;   // ignore enter-key presses.

                char bufFormatted[strlen(buf) + 1];
                if (!firstLine) // avoid newline at file start
                    sprintf(bufFormatted, "\n\0");
                else
                {
                    firstLine = FALSE;
                    sprintf(bufFormatted, "\0");
                }

                sprintf(bufFormatted + strlen(bufFormatted), "%d", atoi(buf));
                write(fdOut, bufFormatted, strlen(bufFormatted));
            }
        }
    }
    else 
    {
        char *buffer;
        size_t size = lseek(fdOut, 0, SEEK_END);
        if((buffer = (char *)malloc(size)) == NULL)
            err_n_exit("Malloc");

        lseek(fdOut, 0, SEEK_SET);
        if(read(fdOut, buffer, size) == -1)
            err_n_exit("Read");

        char delim[] = "\n";
        char *ptr = strtok(buffer, delim), printout[128] = {0}, writeout[24] = {0};
        int sum = 0;
        while(ptr != NULL)
        {
            sprintf(printout + strlen(printout), "%s\n", ptr);
            sum = sum + atoi(ptr);
            ptr = strtok(NULL, delim);
        }
        sprintf(printout + strlen(printout), "%d\n", sum);
        write(STDOUT_FILENO, printout, strlen(printout));
        sprintf(writeout, "\n%d", sum);
        write(fdOut, writeout, strlen(writeout));
        free(buffer);
    }

    /* Sulje */
    close(fdOut);
    exit(EXIT_SUCCESS);
}


void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}