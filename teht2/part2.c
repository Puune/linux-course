#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#define COMPARISONS_AMOUNT 2   //Miksei
#define MAX_SIZE 256
#define TRUE 1
#define FALSE 0

int main(int argc, const char *argv[])
{
    int fds[COMPARISONS_AMOUNT], topLen = 0;
    size_t sizes[COMPARISONS_AMOUNT];
    char *ptrs[COMPARISONS_AMOUNT];

    /* Initialisoi */
    if(argc != (1 + COMPARISONS_AMOUNT))
        err_n_exit("Wrong amount of arguments");

    for (int i=0; i<COMPARISONS_AMOUNT; i++)
    {
        if((fds[i] = open(argv[1+i], O_RDONLY)) < 0) //fopen
            err_n_exit(argv[i+1]);

        sizes[i] = lseek(fds[i], 0, SEEK_END); //size

        if((ptrs[i] = (char *)malloc(sizes[i])) == NULL) //ptr allocation
            err_n_exit("malloc");

        lseek(fds[i], 0, SEEK_SET); //fd reset
        if(read(fds[i], ptrs[i], sizes[i]) < 0) //read into ptrs
            err_n_exit("read");

        if (sizes[i] > topLen)
            topLen = sizes[i];
    }

    /* Samanlaisuuden tarkistus */
    int foundDif = FALSE, diffIndex = 0;
    for (int i=0; i<topLen; i++) 
    {  
        if (foundDif)
            break;
        for (int j=1; j<COMPARISONS_AMOUNT; j++)
        {
            if (ptrs[0][i] != ptrs[j][i])
            {
                diffIndex = i;
                foundDif = TRUE;
                break;
            }
        }
    }
    
    /* Tulosta */
    if (foundDif)
    {   
        char out[MAX_SIZE];
        for (int i=0; i<COMPARISONS_AMOUNT; i++) 
        {
           sprintf(out + strlen(out), "Tiedoston: %s koko on: %d \n", argv[1+i], sizes[i]);
        }

        sprintf(out + strlen(out), "Eroavat merkit olivat kohdassa: %d -> ", diffIndex);
        char diffs[COMPARISONS_AMOUNT];
        for (int i=0; i<COMPARISONS_AMOUNT; i++)
        {
            lseek(fds[i], diffIndex, SEEK_SET);
            read(fds[i], &diffs[i], 1);   
            sprintf(out + strlen(out), "%c, ", diffs[i]);
        }   
        sprintf(out + strlen(out), "\n");
    
        if(write(STDOUT_FILENO, out, strlen(out)) < 0)
            err_n_exit("write");

    } else 
    {
        char *out = "Sisällöt ovat samoja\n";
        write(STDOUT_FILENO, out, strlen(out));
    }

    /* Sulje */
    for(int i=0; i<COMPARISONS_AMOUNT; i++) 
    {
        close(fds[i]);
        free(ptrs[i]);
    }

    exit(EXIT_SUCCESS);
}

void err_n_exit(char *msg) 
{
    perror(msg);
    exit(EXIT_FAILURE);
} 
