#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#define INPUT_WIDTH 10
#define TRUE 1
#define FALSE 0

int main(int argc, char const *argv[])
{
    int fdOut;

    /* Initialisoi */
    if (argc != 2)
        err_n_exit("Give filename as argument");
    else if ((fdOut = creat(argv[1], S_IRWXU)) < 0)
        err_n_exit("Could't create a file");
    

    /* Aja */
    char buf[INPUT_WIDTH];
    char bufFormatted[INPUT_WIDTH + 2];
    int stop = FALSE, status;
    write(STDOUT_FILENO, "Started reading\n", sizeof("Started reading\n"));
    while (!stop)
    {
        status = read(STDIN_FILENO, buf, INPUT_WIDTH);
        if(status == -1) //ERR
            err_n_exit("Error reading input");
        else if (status == 0) //EOF
        {
            write(STDOUT_FILENO, "Stopped writing\n", sizeof("Stopped writing\n"));
            stop = TRUE;
        }
        else
        {
            sprintf(bufFormatted, "%d\n", atoi(buf));
            write(fdOut, bufFormatted, strlen(bufFormatted));
        }
            
    }

    /* Sulje */
    close(fdOut);
    exit(EXIT_SUCCESS);
}


void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}