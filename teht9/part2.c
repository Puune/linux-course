#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define MAX_IN 10

void *thread_factorial(void *param)
{
    int *value = (int *)param;
    int fact = 1;
    for(; *value > 0; --*value)
    {
        fact = fact * *value;
    }
    memcpy(value, &fact, sizeof(int));
    pthread_exit(NULL);
}

void *thread_sum(void *param)
{
    int *value = (int *) param;
    int sum = 0;
    for(int i=1; i < *(int *)value; i++)
    {
        sum = sum + i;
    }
    memcpy(value, &sum, sizeof(int));
    pthread_exit(NULL);
}

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    char buf[MAX_IN];
    pthread_t fact, sum;

    while (1)
    {  
        printf("Kirjoita luku (tai ctrl-c): \n");
        read(STDIN_FILENO, buf, MAX_IN);
    
        int i_fact = atoi(buf);
        int i_sum = atoi(buf);

        if(pthread_create(&fact, NULL, thread_factorial, (void *) &i_fact) != 0)
            err_n_exit("thread_factorial");
        if(pthread_create(&sum, NULL, thread_sum, (void *) &i_sum) != 0)
            err_n_exit("thread_sum");

        pthread_join(fact, NULL);
        pthread_join(sum, NULL);
                
        printf("Factorial: %d\n", i_fact);
        printf("Sum: %d\n", i_sum);
    }
    

    exit(EXIT_SUCCESS);
}
