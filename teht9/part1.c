#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <math.h>

#define SIZE 90
#define THREADS 10
float neliojuuret[SIZE];

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void *t_sqrt(void *i)
{
    printf("%d, ", *((int *) i));

    neliojuuret[*((int *) i)] = sqrt(*((int *) i));

    free(i);
    pthread_exit((void *) 0);
}

int main(int argc, char const *argv[])
{
    pthread_t **threads = (pthread_t **)malloc(THREADS * sizeof(pthread_t *));

    printf("Alkiot suoritusjärjestyksessä:\n");
    for(int i=0; i<SIZE; i++)
    {
        int *cpy = malloc(sizeof(int *));
        memcpy(cpy, &i, sizeof(int));

        if(pthread_create(&threads[i % THREADS], NULL, t_sqrt, (void *) cpy) != 0)
            err_n_exit("pthread_create");
    }
    pthread_join(threads[9], NULL);

    printf("\nNeliöjuuret:\n");
    
    for(int i=0; i<SIZE; i++)
        printf("%0.2f, ", neliojuuret[i]);

    printf("\n");

    free(threads);
    exit(EXIT_SUCCESS);    
}
