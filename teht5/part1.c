#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#define NOT_INFINITE 20
#define MAX_STRING 512

void handler(int sig)
{
    char *msg;
    if(sig == 2)
    {
        msg = "\nSIGINT vastaanotettu\n";
    }
    else if(sig == 15)
    {
        msg = "\nSIGTERM vastaanotettu\n";
    }
     
    size_t size = strlen(msg); 
    write(STDOUT_FILENO, msg, size);
}


int main(int argc, char const *argv[])
{
    int pid = getpid();
    unsigned int seconds = 1;

    //handlers
    struct sigaction act;
    act.sa_handler = handler;
    act.sa_flags = 0;

    sigemptyset(&act.sa_mask);
    sigaction(SIGINT, &act, NULL);
    sigaction(SIGTERM, &act, NULL);
    
    //loop
    for(int i=0; i<NOT_INFINITE; i++)
    {
        printf("pid: %d\n", pid);
        sleep(seconds);
    }

    exit(EXIT_SUCCESS);
}
