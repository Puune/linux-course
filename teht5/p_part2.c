#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

/*
    Parent src file
*/

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);   
}

void handler(int sig)
{
    char *msg = "Vanhempi: SIGUSR1\n";
    write(STDOUT_FILENO, msg, strlen(msg));
}

int main(int argc, char const *argv[])
{   
    if(argc != 2)
        err_n_exit("Usage: arg1 compiled child process code path");

    struct sigaction act;
    act.sa_handler = handler;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    sigaction(SIGUSR1, &act, NULL);

    int pid;
    char tmp[100]; memset(tmp, '\0', 100);
    sprintf(tmp, "%d", getpid());
    char *p_pid = &tmp;

    if((pid = fork()) < 0)
        err_n_exit("fork");

    if(pid == 0)    /* Lapsi */
    {
        if(execl(argv[1], p_pid, (char *)NULL)  < 0)
            err_n_exit("execl");
    } 
    else    /* Vanhempi */
    {
        pause();

        /* Halvin tapa saada lapsen pid (ainakin omalla systeemilläne) */
        int pidhack = getpid() + 1;
        kill(pidhack, SIGUSR1);

        printf("Vanhempi: lopetan..\n");
    }

    exit(EXIT_SUCCESS);
}
