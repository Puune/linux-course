#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#define MAX_STRING 1024

/*
    Child src file
*/

void handler(int sig)
{
    char *msg = "Lapsi: SIGUSR1\n";
    write(STDOUT_FILENO, msg, strlen(msg));
}

int main(int argc, char const *argv[])
{
    struct sigaction act;
    act.sa_handler = handler;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    sigaction(SIGUSR1, &act, NULL);

    int p_pid = atoi(argv[0]);
    unsigned int t_sleep = 3u;
    char msg[MAX_STRING];
    sprintf(msg, "Lapsi: minä tulostan..\n");
    write(STDOUT_FILENO, msg, strlen(msg));
    sleep(t_sleep);

    printf("Lapsi: kill %d\n", p_pid);
    kill(p_pid, SIGUSR1);
    pause();
    printf("Lapsi: lopetan..\n");
    exit(EXIT_SUCCESS);
}
