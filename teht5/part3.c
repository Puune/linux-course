#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#define NOT_INFINITE 100

static int count = 0;

void handler(int sig)
{
    char *msg = "SIGCHLD vastaanotettu\n";
    write(STDOUT_FILENO, msg, strlen(msg));
    count++;
}

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    int pid, p_pid = getpid();
    for(int i=1; i<4; i++)
    {
        if((pid = fork()) < 0)
            err_n_exit("fork");
               
        if(pid == 0)
        {
            sleep(i * 2);
            _exit(EXIT_SUCCESS);
        }
    }

    struct sigaction act;
    act.sa_handler = handler;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    sigaction(SIGCHLD, &act, NULL);

    unsigned int index = 0u, t_sleep = 1u;
    while(count < 3 & index < NOT_INFINITE)
    {
        printf("%d\n", index++);
        sleep(t_sleep);
    }

    printf("Vahempi: exit..\n");
    exit(EXIT_SUCCESS);
}
