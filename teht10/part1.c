#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <math.h>

#define LENGTH 10000
#define MAX_VAL 999

int arr[LENGTH];
pthread_rwlock_t lock = PTHREAD_RWLOCK_INITIALIZER;

void *thread_rand(void *arg)
{
    for(int t=0; t<10; t++)
    {
        pthread_rwlock_wrlock(&lock);
        write(STDOUT_FILENO, "rand... ", strlen("rand ... "));
        for(int i=0; i<LENGTH; i++)
        {
            arr[i] = rand() % 1000;
        }
        write(STDOUT_FILENO, " ... done \n", strlen("... done \n "));
        pthread_rwlock_unlock(&lock);
        sleep(2);
    }
    for(int i=0; i<LENGTH; i++)
    {
        arr[i] = -1;
    }
    pthread_exit(NULL);
}

void *thread_calc(void *arg)
{
    int index = (int) arg;
    double val_last = 0;
    double val = 0;
    while (1)
    {
        pthread_rwlock_rdlock(&lock);
        //printf("calc\n");

        /**
         * Method 1 - static
         * sum every 100 average of 10 000 and divide by 100 then again
         
        int incrementer = 0;
        for(int i=0; i < LENGTH; i++)
        {
            incrementer += arr[i];
            if((i+1) % 100 == 0)
            {
                val += incrementer / 100;
                incrementer = 0;
            }
        }

        val /= 100;
        printf("method 1: %.5f\n", val);
        val = 0;
          */
        for(int i=0; i<LENGTH; i++)
        {
                /*
                    Method 2 - dynamic (all my homies hate big integers)
                    new average = ( previous average * previous amount of members + new member ) / current amount of members 
                */
                val = (val * (i) + arr[i]) / (i+1);
        }

        if(fabs(val-val_last) > 0.00001f)
            printf("%d: Average %.5f, delta = %.5f \n", index, val, fabs((val-val_last)));
        
        pthread_rwlock_unlock(&lock);

        if(val < 0)
            break;
    
        val_last = val;
        val = 0;

        sleep(1);
    }

    pthread_exit(NULL);
}

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    pthread_t t_rand, t_calc;

    if(pthread_create(&t_rand, NULL, thread_rand, NULL) != 0)
        err_n_exit("Thread rand");

    for(int i=0; i<5; i++)
    {
        if(pthread_create(&t_calc, NULL, thread_calc, (void *) (i+1)) != 0)
            err_n_exit("Thread calc");
    }

    pthread_join(t_rand, NULL);
    pthread_join(t_calc, NULL);

    exit(EXIT_SUCCESS);
}
