#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define T_SLP_MAX 2000000
#define T_SLP_MIN 100

int x = 0;
int count = 0;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void *thread_add(void *arg)
{
    unsigned int t_time = 0;
    while(1)
    {
        t_time = (rand() & (T_SLP_MAX-T_SLP_MIN)) + T_SLP_MIN;
        usleep(t_time);

        pthread_mutex_lock(&lock);
        printf("add: %d\n", ++x);
        pthread_mutex_unlock(&lock);
        pthread_cond_signal(&cond);
    }
}

void *thread_subtract(void *arg)
{
    unsigned int t_time = 0;
    while (1)
    {
        t_time = (rand() & (T_SLP_MAX-T_SLP_MIN)) + T_SLP_MIN;
        usleep(t_time);

        pthread_mutex_lock(&lock);
        printf("sub: %d\n", --x);
        pthread_mutex_unlock(&lock);
        pthread_cond_signal(&cond);
    }
    
}

void *thread_reset(void *arg)
{
    while(count < 3)
    {
        while(x <= 2 && x >= -2)
            pthread_cond_wait(&cond, &lock);

        printf("*** reset: %d \n", x);
        x = 0;
        count++;
    }
}

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    pthread_t sub, add, res;

    if(pthread_create(&sub, NULL, thread_subtract, NULL) != 0)
        err_n_exit("t_sub");
    if(pthread_create(&add, NULL, thread_add, NULL) != 0)
        err_n_exit("t_add");
    if(pthread_create(&res, NULL, thread_reset, NULL) != 0)
        err_n_exit("t_reset");
    
    pthread_join(res, NULL);

    pthread_cancel(sub);
    pthread_cancel(add);

    exit(EXIT_SUCCESS);
}
