#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>

#define MAX_STRING 1024
#define P_COUNT 2

void err_n_exit(char *msg, pid_t pid)
{
    perror(msg);
    pid == 0 ? _exit(EXIT_FAILURE) : exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    pid_t pid, pid_emo = getpid();
    int myid = 0;

    while(myid < P_COUNT)
    {
        if((pid = fork()) < 0)
            err_n_exit("fork", -1);
        
        if(pid == 0)
            break;

        myid++;
    }

    char buf[MAX_STRING];
    if(pid == 0) /* Lapsiprosessi */
    {   
        uint t_sleep = 3;
        if(myid == 0)
        {   
            sprintf(buf, "Eka lapsi %d valmis \n", myid);
            write(STDOUT_FILENO, buf, strlen(buf));
            _exit(1);
        }
        else 
        {
            sleep(t_sleep);
            sprintf(buf, "Toka lapsi %d valmis \n", myid);
            write(STDOUT_FILENO, buf, strlen(buf));
            _exit(2);
        }
    } 
    else /* Emoprosessi */
    {
        int status1, status2;   
        waitpid(0, &status1, 0);   
        waitpid(0, &status2, 0);
        status1 = WEXITSTATUS(status1);
        status2 = WEXITSTATUS(status2);

        printf("status eka %d \n", status1);
        printf("status toka %d \n", status2);
    }


    exit(EXIT_SUCCESS);
}
