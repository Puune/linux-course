#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAX_STRING 1024

void err_n_exit(char *msg, pid_t pid)
{
    perror(msg);
    pid == 0 ? _exit(EXIT_FAILURE) : exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    int pp[2];
    pid_t pid = getpid();
    static char template[] = "tempXXXXXX";
    char fname[MAX_STRING];
    strcpy(fname, template);

    if(pipe(pp) < 0)
        err_n_exit("pipe", pid);

    if((pid = fork()) < 0)
        err_n_exit("fork", pid);

    if(pid == 0) /* Lapsi */
    {
        char buf[1024];
        sprintf(buf, "This is child writing..\n..and done\n");
        write(pp[1], buf, strlen(buf)+1);
        close(pp[1]);
        printf("Child exits..\n");
        _exit(EXIT_SUCCESS);
    }
    else /* vanhempi */ 
    {  
        int wstatus;
        if((wstatus = waitpid(-1, &wstatus, 0)) < 0)
            err_n_exit("waitpid", pid);

        char buf[1024];
        if(read(pp[0], buf, MAX_STRING) < 0)
            err_n_exit("read", pid);

        close(pp[0]);

        printf("reading from parent:\n%s \n", buf);
    }

    exit(EXIT_SUCCESS);
}
