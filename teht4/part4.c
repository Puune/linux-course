#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#define MAX_STRING 1024

void err_n_exit(char *msg, pid_t pid)
{
    perror(msg);
    pid == 0 ? _exit(EXIT_FAILURE) : exit(EXIT_FAILURE);
} 

int main(int argc, char const *argv[])
{
    int fd;
    pid_t pid = getpid();
    static char template[] = "tempXXXXXX";
    char fname[MAX_STRING];
    strcpy(fname, template);

    if((fd = mkstemp(fname)) < 0)
        err_n_exit("mkstemp", -1);

    if((pid = fork()) < 0)
        err_n_exit("fork", pid);


    if(pid == 0) /* Lapsi */
    {
        char buf[1024];
        sprintf(buf, "This is child writing..\n..and done\n");
        write(fd, buf, strlen(buf)+1);
        printf("Child exits..\n");

        fcntl(fd, 12, fname); //12 == F_GETPATH, ei löydy linuxini kirjastosta jostain syystä
        unlink(fname);
        _exit(EXIT_SUCCESS);
    }
    else /* vanhempi */ 
    {  
        int wstatus;
        if((wstatus = waitpid(-1, &wstatus, 0)) < 0)
            err_n_exit("waitpid", pid);

        lseek(fd, 0, SEEK_SET);
        char buf[1024];
        if(read(fd, buf, MAX_STRING) < 0)
            err_n_exit("read", pid);

        printf("reading from parent:\n%s \n", buf);
        printf("Unlink tuhoaa tiedoston vasta kun kaikki linkit siihen on katkaistu, siksi voidaan unlink() kutsua myös lapsiprosessista\n");
    }


    exit(EXIT_SUCCESS);
}
