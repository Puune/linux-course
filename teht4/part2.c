#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAX_STRING 1024

//Jokin lähde kertoi, että lapsi pitää lopettaa (konteksti on tärkeä) _exit() käyttäen
void err_n_exit(char *msg, pid_t pid)
{
    perror(msg);
    pid == 0 ? _exit(EXIT_FAILURE) : exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
    pid_t pid, pid_parent = getpid();
    
    if((pid = fork()) < 0)
        err_n_exit("Fork", getpid());

    /*  Lapsiprosessi */
    if (pid == 0) 
    {
        char buf[MAX_STRING]; uint t_sleep = 5;
        sleep(t_sleep);

        sprintf(buf, "Olen lapsi, pid: %d, emon pid: %d \n", pid, pid_parent);
        if(write(STDOUT_FILENO, buf, strlen(buf)) < 0)
            err_n_exit("write", pid);

        sprintf(buf, "Nyt lapsen omistaa init \0");
        if(write(STDOUT_FILENO, buf, strlen(buf)) < 0)
            err_n_exit("write", pid);

        _exit(EXIT_SUCCESS);
    }
    else  /* emoprosessi */
    {
        char buf[MAX_STRING];
        sprintf(buf, "Olen emo, pid: %d\n", pid);
        if(write(STDOUT_FILENO, buf, strlen(buf)) < 0)
            err_n_exit("write", pid);     
    }

    exit(EXIT_SUCCESS);
}