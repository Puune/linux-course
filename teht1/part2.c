#include "stdio.h"
#include "stdlib.h"
#include "string.h"

int main(int argc, char const *argv[], char *envp[])
{
    char *DEF_OUT = "EI_ASETETTU";

    if (argc > 1) 
    {
        char *val = getenv(argv[1]);
        
        if (val != NULL) 
        {
            printf("%s \n", val);
        } 
        else 
        {
            for (int i=0; i<sizeof envp; i++) 
            {
                printf("%s \n", envp[i]);
            }
            printf ("%s=%s \n", argv[1], DEF_OUT);
        }   
    }
    return 0;
}
