#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "string.h"

int main(int argc, char *argv[])
{
    if (argc<2) {
        return 0;
    }

    float num = atof(argv[1]);
    int option;
    while ((option = getopt(argc, argv, "a:s:d:m:")) != -1)
    {
        switch (option)
        {
        case 'a':
            num = num + atof(optarg);
            break;

        case 's':
            num = num - atof(optarg);
            break;

        case 'd':
            if (atof(optarg) == 0) 
            {
                printf("Division by zero not allowed, aborting..\n");
                return 0;
            } else 
            {
                num = num / atof(optarg);
            }
            break;

        case 'm':
            num = num * atof(optarg);
            break;
        
        default:
            printf ("Option %s is undefined", option);
        }
    }

    printf("%f \n", num);
    return 0;
}
