#include "stdio.h"
#include "string.h"

int main(int argc, char const *argv[])
{
    if (argc < 2) 
    {
        printf("Et antanut komentorivillä yhtään argumenttia\n");
    } 
    else 
    {
        printf("%s", "Ohjelman nimi on: ");
        printf(strcat(argv[0], " \n"));
        
        printf("Annoit ohjelmalle %d argumenttia\n", (argc-1));

        //Sometimes 1st user argument prints as '\n' for some reason??
        for (int i=(argc-1); i>-1; i--) 
        {
            printf("%d %s\n",i, argv[i]);
        }
    }

    return 0;
}