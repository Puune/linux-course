#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>

int cont = 1;

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void handler(int sig)
{
    cont = 0;
}

int main(int argc, char const *argv[])
{
    pid_t pid;
    int fd[2];
    pipe2(fd, O_NONBLOCK);

    if((pid = fork()) < 0)
        err_n_exit("fork");
    
    if(pid == 0)
    {
        char buf[] = "Message from child\n";
        close(fd[0]);
        for(int i=0; i<10; i++)
        {
            write(fd[1], buf, strlen(buf) + 1);
            sleep(1);
        }
        close(fd[1]);
    }
    else
    {
        close(fd[1]);
        char buf[254];

        struct sigaction act;
        act.sa_handler = handler;
        act.sa_flags = 0;
        sigemptyset(&act.sa_mask);
        sigaction(SIGCHLD, &act, NULL);

        int status, fd_in = STDIN_FILENO, flags;
        flags = fcntl(STDIN_FILENO, F_GETFL);
        fcntl(fd_in, F_SETFL, flags | O_NONBLOCK);

        while(cont)
        {
            status = read(fd[0], buf, 254);
            if(status != -1)
            {
                write(STDOUT_FILENO, buf, strlen(buf));
                *buf = 0;
            }

            if(read(fd_in, buf, 256) > 0)
            {
                write(STDOUT_FILENO, buf, strlen(buf));
            }  
        }
        close(fd[0]);
    }

    exit(EXIT_SUCCESS);
}
