#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>

int cont = 1;

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void handler(int sig)
{
    cont = 0;
}

int main(int argc, char const *argv[])
{
    pid_t pid;
    fd_set rset;
    struct timeval tvptr = {0, 0};
    int fd = mkfifo("/tmp/fifo", O_RDWR);
;

    FD_ZERO(&rset);

    if((pid = fork()) < 0)
        err_n_exit("fork");
    
    if(pid == 0)
    {
        FD_SET(fd, &rset);

        char buf[] = "Message from child\n";
        for(int i=0; i<10; i++)
        {
            write(fd, buf, strlen(buf) + 1);
            sleep(1);
        }
    }
    else
    {
        char buf[254];

        struct sigaction act;
        act.sa_handler = handler;
        act.sa_flags = 0;
        sigemptyset(&act.sa_mask);
        sigaction(SIGCHLD, &act, NULL);

        int status, fd_in = STDIN_FILENO, flags;
        flags = fcntl(STDIN_FILENO, F_GETFL);
        fcntl(fd_in, F_SETFL, flags | O_NONBLOCK);

        FD_SET(fd, &rset);
        FD_SET(fd_in, &rset);

        while(cont)
        {
            if(select(1+1, &rset, NULL, NULL, NULL) == -1)
                err_n_exit("select");

            if(FD_ISSET(fd_in, &rset))
            {
                printf("STDIN\n");
                FD_CLR(fd_in, &rset);
            }

            if(FD_ISSET(fd, &rset))
            {
                printf("PIPE\n");
                FD_CLR(fd, &rset);
            }

            /*
            if(status != -1)
            {
                write(STDOUT_FILENO, buf, strlen(buf));
                *buf = 0;
            }

            if(read(fd_in, buf, 256) > 0)
            {
                write(STDOUT_FILENO, buf, strlen(buf));
            }  
            */
        }
    }

    exit(EXIT_SUCCESS);
}