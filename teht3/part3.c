#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

#define TRUE 1
#define FALSE 0
#define MAX_STRING 1024
#define MAX_DEPTH 10

/* 
    DISCLAIMER: has bugs
*/

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

/**
 * Character matcher
 * Try to find match in two sets of strings given
 * param1 matcher - what to find
 * param2 subject - where to find it
 */
int match(char *matcher, char *subject)
{
    int startsWithWildcard = *matcher == '*';
    char *mcpy = matcher, *scpy = subject;
    size_t len = strlen(matcher) - startsWithWildcard;
    int failure = 0;

    if (startsWithWildcard == FALSE & *matcher != *subject) //Early prune
        return FALSE;

    while (*scpy != '\0')
    {
        if(*mcpy == '*')    // skip wildcard
        {
            mcpy++;
        }
        else if(*mcpy == *scpy)  //Matching
        {
            len--;
            mcpy++;
            scpy++;
        } 
        else if(*mcpy != *(matcher+startsWithWildcard))  //reset after matching
        {
            mcpy = matcher + startsWithWildcard;
            len = strlen(matcher) - startsWithWildcard;
        }
        else //no match
        {
            scpy++;
        }

        if(len == 0) // matched whole string
            return TRUE;
    }

    return FALSE;
}

/**
 * Content matcher
 * Find string in file, return TRUE/FALSE
 */
int etsi(int fd, char *mjono)
{
    char *buf, *ptr = mjono;                                     //buffer / movable ptr to buffer / movable ptr to mjono
    int *charsLeft = strlen(mjono), completeMatch = FALSE; //amount of chars left to match / bool / filesize
    off_t sizeofbuf = lseek(fd, 0, SEEK_END);
    if (sizeofbuf == 0)
        return FALSE;

    if ((buf = (char *)malloc(sizeofbuf)) == NULL)
        err_n_exit("Malloc");

    lseek(fd, 0, SEEK_SET);
    if (read(fd, buf, sizeofbuf) <= 0)
        err_n_exit("Read");

    completeMatch = match(mjono, buf);
    free(buf);
    return completeMatch;
}

/**
 * File seeker
 * Iterate through all items in dir given, dive recursively into subdirs (upto safety depth)
 */
void getMatches(char *dir, int *depthToGo, char *fmatcher, int *totalcount, char **matched)
{
    DIR *current;
    struct dirent *dent;
    struct stat sbuf;
    int lenstart = *totalcount;
    --*depthToGo; // current depth
    printf("dir depth %d\n", MAX_DEPTH-*depthToGo); //D

    if ((current = opendir(dir)) == NULL)
        err_n_exit("Could not open directory");

    while ((dent = readdir(current)) != NULL)
    {
        if (*dent->d_name == *".")
            continue;

        size_t pathlen = strlen(dir) + strlen(dent->d_name) + 1;
        char curpath[pathlen];
        sprintf(curpath, "%s/%s", dir, dent->d_name);

        if (stat(curpath, &sbuf) != 0)
            err_n_exit("stat");

        if (S_ISDIR(sbuf.st_mode) & depthToGo != 0)
        {
            printf("DIR! %s \n", dent->d_name); //D
            int oldsize = *totalcount;
            getMatches(curpath, depthToGo, fmatcher, totalcount, matched); // epic recursion action
        }
        else if (S_ISDIR(sbuf.st_mode))
        {
            printf("Max dir depth (%d) reached!", MAX_DEPTH);
        }
        else
        {
            printf("%s ? %s \n", fmatcher, dent->d_name); //D
            if (match(fmatcher, dent->d_name) == TRUE)
            {
                printf("matched\n"); //D
                int newsize = ((*totalcount + 1) * sizeof(char *));
                if ((matched = realloc(matched, newsize)) != NULL)
                {
                    printf("reallocated\n"); //D
                    char *toadd;
                    size_t size = strlen(curpath);

                    if ((toadd = (char *)malloc(size + 1)) == NULL)
                        err_n_exit("malloc");

                    printf("mallocated \n"); //D
                    memset(toadd, '\0', size);

                    toadd = strcpy(toadd, curpath);

                    printf("index %lu \n", *totalcount); //D
                    matched[*totalcount] = toadd;
                    printf("done %s \n\n", matched[*totalcount]); //D
                    (*totalcount)++;                            //Commit addition to count
                }
                else
                    err_n_exit("realloc");
            }
        }
    }

    closedir(current);
    printf("Exit match()\n");
    for (int i = 0; i < *totalcount; i++)
    {
        printf("for loop match - %d: %s\n", i, matched[i]); //D
    }

    ++*depthToGo; //Back to previous depth
    return;
}

int main(int argc, char const *argv[])
{
    if (argc != 3)
        err_n_exit("usage \narg1: searh string, \narg2: file matcher\n");

    char **matched = malloc(0);
    char buf[MAX_STRING];
    int *totalcount, init1 = 0; totalcount = &init1;
    int *depthToGo, init2 = MAX_DEPTH; depthToGo = &init2;

    getcwd(buf, MAX_STRING);
    printf("%s \n", buf); //D
    getMatches(buf, depthToGo, argv[2], totalcount, matched);
    // Kun haku wildcardilla tuottaa liian monta tulosta, tässä vaiheessa viimeistään kai tapahtuu matched[] korruptio.

    char output[MAX_STRING];
    memset(output, '\0', MAX_STRING);

    for (int i = 0; i < *totalcount; i++)
    {
        char *ptr1 = buf, *ptr2 = matched[i];
        while (*ptr1 != '\0')
        {
            ptr1++;
            ptr2++;
        }
        ptr2++;

        printf("To read: %s \n", ptr2); //D
        int fd;
        if ((fd = open(ptr2, O_RDONLY)) < 0)
            err_n_exit("open");

        sprintf(output + strlen(output), "Tiedosto %s : ", ptr2);
        if(etsi(fd, argv[1]))
            sprintf(output + strlen(output), "Löytyi\n");
        else 
            sprintf(output + strlen(output), "Ei löytynyt\n");

        close(fd);
    }

    printf("********\n%s \n********\n", output);

    //printf("Close %d\n", *totalcount);    //D
    for (int i = 0; i < *totalcount; i++)
    {
        //printf("%s \n", matched[i]);  //D
        free(matched[i]);
    }
    free(matched);

    exit(EXIT_SUCCESS);
}
