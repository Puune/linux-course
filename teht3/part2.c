#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define TRUE 1
#define FALSE 0

int main(int argc, char const *argv[])
{
    int fd, success;

    if(argc != 3)
        err_n_exit("usage -> arg1: fname, arg2: search");
    else if((fd = open(argv[1], O_RDONLY)) < 0)
        err_n_exit("Failed to open file");


    success = etsi(fd, argv[2]);

    if(success) 
    {
        char *buf = "Haettu merkkijono löytyi tiedostosta\n";
        write(STDOUT_FILENO, buf, strlen(buf));
    } else 
    {
        char *buf = "Haettua merkkijonoa ei löydetty\n";
        write(STDOUT_FILENO, buf, strlen(buf));
    }

    close(fd);
    exit(EXIT_SUCCESS);
}

int etsi(int fd, char *mjono)
{
    char *buf, *bufcpy, *ptr = mjono; //buffer / movable ptr to buffer / movable ptr to mjono
    int charsLeft = strlen(mjono), completeMatch = FALSE, size; //amount of chars left to match / bool / filesize

    if((buf = (char *)malloc((size = lseek(fd, 0, SEEK_END)))) == NULL)
        err_n_exit("Malloc");

    lseek(fd, 0, SEEK_SET);
    if(read(fd, buf, size) <= 0)
        err_n_exit("Read");

    
    bufcpy = buf;
    while (*bufcpy != 0)
    {
        if(*ptr == *bufcpy)
        {
            //match, continue
            ptr++;
            charsLeft--;
            bufcpy++;
        } else if (*ptr != *mjono)
        {
            //reset
            //dont move bufcpy, so next iter can start matching again from current pos
            ptr = mjono;
            charsLeft = strlen(mjono);
        } else 
        {
            //no match, continue
            bufcpy++;
        }

        if (charsLeft == 0)
        {
            completeMatch = TRUE; 
            break;
        }
    }

    free(buf);
    return completeMatch;
}

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}
