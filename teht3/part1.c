#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#define TRUE 1
#define FALSE 0
#define MAX_PATH 1024

int main(int argc, char const *argv[])
{
    int fd1, fd2;
    if(argc != 3) 
        err_n_exit("Please give 2 fNames as arguments");

    if((fd1 = open(argv[1], O_RDONLY)) < 0) 
        err_n_exit("Couldn't open file 1");

    if((fd2 = open(argv[2], O_RDONLY)) < 0) 
        err_n_exit("Couldn't open file 2");

    int same = sameFile(fd1, fd2);
    if(same == TRUE)
        printf("Same files\n");
    else 
        printf("Different files\n");

    close(fd1);
    close(fd2);
    exit(EXIT_SUCCESS);
}

/*
    Pelkillä file descriptoreilla ei mahdollista erottaa linkkiä kiinteästä tiedostosta? fstat
    seuraa linkkiä sen kohteeseen.
*/
int sameFile(int fd1, int fd2)
{
    struct stat st1, st2;
    if(fstat(fd1, &st1) < 0)
        err_n_exit("Stat file1");

    if(fstat(fd2, &st2) < 0)
        err_n_exit("Stat file2"); 
 
    if(st1.st_size != st2.st_size) // same size?
        return FALSE;

    if(st1.st_ino != st2.st_ino) // same inode number?
        return FALSE;

    if(st1.st_mode != st2.st_mode) // same mode?
        return FALSE;

    if(st1.st_dev != st2.st_dev) // same device?
        return FALSE;    

    return TRUE;
}

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}
