#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

#define MAX_STRING 1024
#define FALSE 0
#define TRUE 1

void err_n_exit(char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

/**
 * Character matcher
 * Try to find match in two sets of strings given
 * param1 matcher - what to find
 * param2 subject - where to find it
 */
int match(char *matcher, char *subject)
{
    int startsWithWildcard = *matcher == '*';
    char *mcpy = matcher, *scpy = subject;
    size_t len = strlen(matcher) - startsWithWildcard;
    int failure = 0;

    if (startsWithWildcard == FALSE & *matcher != *subject) //Early prune
        return FALSE;

    while (*scpy != '\0')
    {
        if(*mcpy == '*')    // skip wildcard
        {
            mcpy++;
        }
        else if(*mcpy == *scpy)  //Matching
        {
            len--;
            mcpy++;
            scpy++;
        } 
        else if(*mcpy != *(matcher+startsWithWildcard))  //reset after matching
        {
            mcpy = matcher + startsWithWildcard;
            len = strlen(matcher) - startsWithWildcard;
        }
        else //no match
        {
            scpy++;
        }

        if(len == 0) // matched whole string
            return TRUE;
    }

    return FALSE;
}

int main(int argc, char const *argv[])
{     
    if(argc != 3)
        err_n_exit("Usage: Arg1: kansio A, Arg2: kansio B");
    
    DIR *dira, *dirb;
    struct dirent *adent, *bdent;
    char output[MAX_STRING], path[MAX_STRING]; memset(output, '\0', MAX_STRING); getcwd(&path, MAX_STRING);
    char *apath = (char *)malloc((strlen(path) + strlen(argv[1])) + 1);
    char *bpath = (char *)malloc((strlen(path) + strlen(argv[2])) + 1);    
    sprintf(apath, "%s/%s", path, argv[1]);
    sprintf(bpath, "%s/%s", path, argv[2]);

    if((dira = opendir(apath)) == NULL)
        err_n_exit("Dir A open failed");
    if((adent = readdir(dira)) == NULL)
        err_n_exit("Dir A read failed");    

    while((adent = readdir(dira)) != NULL) 
    {
        if(*(adent->d_name) == '.')
            continue;

        if((dirb = opendir(bpath)) == NULL)
            err_n_exit("Dir B open failed");

        while((bdent = readdir(dirb)) != NULL) 
        {
            if(*(bdent->d_name) == '.')
                continue;

            //Alla halpa temppu olla muuttamatta ylläolevaa match funktiota
            if(match(adent->d_name, bdent->d_name) && match(bdent->d_name, adent->d_name))
            {
                sprintf(output + strlen(output), "Tiedosto %s lötyy molemmista hakemistoista.\n", adent->d_name);       
                break;
            }
        }
        
        closedir(dirb);
    }

    printf("%s\n", output);

    closedir(dira);
    free(apath); free(bpath);
    exit(EXIT_SUCCESS);
}
